# SSRLoS API

早期的功能设计是符合 `cocos2d-x` 风格的，后期 `Creator` 开始流行，在移植的同时，参考了组件化的设计方式。**但是有部分模块还没有完全的组件化**。

### 注意

这里的所提到的 `API` 接口以及演示程序，都是基于 `cocos2d-x` 的，但是由于核心的算法实现 `Creator` 和 `cocos2d-x` 版本是完全一直的，所以这些 `API` 基本也都是可以在 `Creator` 项目中直接使用的。

`jsdoc` 生成的文档略落后于实际开源的代码。

`jsdoc` 生成的文档不包含 `shadow` 相关内容，因为 `shadow` 的功能暂时为 `实验版`。

`API` 文档还在完善中，具体接口，使用方法可以参见对应的 `Demo`。

## Core

`LoS` 的核心组件，基于 `ray tracing` 算法，计算可视范围，以及其他一些相关数据。

###  Load - 挂载组件

任何 `cc.Node` 对象，都可以通过添加该组件，来获取 `视野` 的功能:

```javascript
var robot = new cc.Sprite("...");
scene.addChild(robot);
var losCoreComponent = robot.addComponent(ssr.LoS.Component.Core, this);
```

### Setup - 配置组件

在添加完 `LoS` 组件后，就可以通过各种接口来对组件进行配置了，这里列举一些主要的接口。

#### Owner - 所有者

设置组件的所有者，`addComponent` 时会默认绑定，后面也通过该接口更改

```JavaScript
losCoreComponent.setOwner(node);
```

#### Mode - 模式

组件支持的视野范围模式主要有四种:

. `Unlimited Range` 不限制视线距离，不限制视线角度: `360°`	

. `Limited Range Full Angle` 限制视线距离，不限制视线角度: `360°`

. `Limited Range Non Reflex Angle`限制视线距离，限制视线角度: `(0°, 180]`

. `Limited Range Reflex Angle`限制视线距离，限制视线角度: `(180°, 360)`

**注意:** 

虽然模块提供了 `setMode` 接口来切换模式，但是通常只用于切换到 `Unlimited Range` 模式的场景。

其余模式的进入，会在进行对应参数的设置时，自动进入:

```javascript
// 设置视野范围，调用后自动进入 LIMITED_RANGE_WITH_FULL_ANGLE 模式
losCoreComponent.setRadius(value);
// 设置视野的夹角范围 (单位: 角度)
// 调用后自动进入 LIMITED_RANGE_WITH_REFLEX_ANGLE / NON_REFLEX_ANGLE 模式
losCoreComponent.setCentralAngle(value);
// 设置视野的可视范围大小，仅适用于 UNLIMITED_RANGE 模式
losCoreComponent.setSightSize(value);
// 设置视野的可视范围，仅适用于 UNLIMITED_RANGE 模式
losCoreComponent.setSightRect(value);
```

<iframe src="https://codesandbox.io/embed/ssrlos-demo-modes-yxbgj?fontsize=14&hidenavigation=1&theme=dark&view=preview"
     style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;"
     title="ssrlos-demo-modes"
     allow="accelerometer; ambient-light-sensor; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; vr; xr-spatial-tracking"
     sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"
   ></iframe>

#### Obstacle - 障碍物

配置好视野模式和一些参数之后，就需要添加障碍物了。

```javascript
// 添加障碍物 用于视野计算
losCoreComponent.addObstacle(node);
// 删除之前添加的障碍物
losCoreComponent.removeObstacle(node);
// 删除所有已添加的障碍物
losCoreComponent.removeAllObstacles();
// 获取所有已添加的障碍物
losCoreComponent.getObstacles();
```

### Update - 更新

在配置完以后，就可以通过组件，获取视野相关的数据信息了。

```javascript
// 在需要时调用，更新视野范围数据，通常需要每帧调用
losCoreComponent.update();
```

### Output - 输出

在调用更新视野的接口之后，就可以获取到相关的数据了。

```javascript
// 获取实际视野范围
losCoreComponent.getSightArea();
// 获取实际阻挡视野的边的信息
losCoreComponent.getBlockingEdgeArray();
// 获取所有射线的信息
losCoreComponent.getRayArray();
// 获取所有潜在阻挡视野边的信息
losCoreComponent.getPotentialBlockingEdgeArray();
// 获取所有实际可视边的信息
losCoreComponent.getVisibleEdgeArray();
// 获取所有交点的信息
losCoreComponent.getHitPointArray();
// 获取视野顶点信息
losCoreComponent.getSightVertArray();
```

## Obstacle

障碍物是 `Core` 组件计算的重要信息之一。

### Load - 挂载组件

通过组件的设计，可以方便的将任何对象升级为障碍物。

引擎中的任何 `cc.Node` 对象，都可以成为障碍物。

```javascript
var wall = new cc.Sprite("...");
scene.addChild(wall);
var losObstacleComponent = wall.addComponent(ssr.LoS.Component.Obstacle);
```

### Setup - 配置组件

创建好的障碍物以后，就要对其进行配置了。

#### 顶点信息

`TBD`

#### 控制属性

`TBD`

<iframe src="https://codesandbox.io/embed/ssrlos-demo-dirty-detection-eft0n?fontsize=14&hidenavigation=1&theme=dark&view=preview"
     style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;"
     title="ssrlos-demo-dirty-detection"
     allow="accelerometer; ambient-light-sensor; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; vr; xr-spatial-tracking"
     sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"
   ></iframe>

## Render

渲染相关组件，通过创建使用不同的组件，来对不同的对象进行渲染。

#### Create - 创建

```javascript
// 创建一个用于渲染射线的组件，然后对渲染效果的一些属性进行配置
var losComponentRenderRay = new ssr.LoS.Component.RenderRay(loSComponentCore);
losComponentRenderRay.setup(TRANSPARENT, 1, YELLOW);
this.addChild(losComponentRenderRay);
// 可用的渲染组件有很多，基本是和算法的输出一一对应的
ssr.LoS.Component.RenderRay
ssr.LoS.Component.RenderPotentialBlockingEdge
ssr.LoS.Component.RenderBlockingEdge
ssr.LoS.Component.RenderVisibleEdge
ssr.LoS.Component.RenderSightArea
ssr.LoS.Component.RenderHitPoint
ssr.LoS.Component.RenderSightVert
```

### Update - 更新

创建好了渲染对象后，需要配合 `Core` 组件，对其进行一些控制:

```javascript
// 所有 render 组件都有 plot，方法，通过调用进行渲染
if (losCoreComponent.update()) {
  	// 如果 Core 有更新，则更新渲染
  	losComponentRenderXXX.plot();
}
// 所有 render 组件都有 enable / disable，方法，默认情况下为 disable
losComponentRenderXXX.disable();
losComponentRenderXXX.enable();
```

<iframe src="https://codesandbox.io/embed/ssrlos-demo-output-render-nwij2?fontsize=14&hidenavigation=1&theme=dark&view=preview"
     style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;"
     title="ssrlos-demo-output-render"
     allow="accelerometer; ambient-light-sensor; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; vr; xr-spatial-tracking"
     sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"
   ></iframe>

## Mask

`Render` 部分组件主要是对算法的输出进行直接的渲染展示，意义多为调试，当然用户也可以自由的进行定制。

相比之下，`Mask` 则是对 `Render` 中的 `SightArea` 的渲染方式，进行了定制，达到**只显示光源可见范围**的效果。

### Create - 创建

```javascript
var losMaskComponent = new ssr.LoS.Component.Mask(BLACK);
this.addChild(losMaskComponent);
```

<iframe src="https://codesandbox.io/embed/ssrlos-demo-mask-gihmn?fontsize=14&hidenavigation=1&theme=dark&view=preview"
     style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;"
     title="ssrlos-demo-mask"
     allow="accelerometer; ambient-light-sensor; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; vr; xr-spatial-tracking"
     sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"
   ></iframe>

### Target - 目标

创建了 `Mask` 组件以后，就要为其添加需要渲染的目标了，支持多个目标同时渲染。

```javascript
losMaskComponent.addTarget(loSComponentCoreA);
losMaskComponent.addTarget(loSComponentCoreB);
losMaskComponent.addTarget(loSComponentCoreC);
```

<iframe src="https://codesandbox.io/embed/ssrlos-demo-multi-masks-f68bq?fontsize=14&hidenavigation=1&theme=dark&view=preview"
     style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;"
     title="ssrlos-demo-multi-masks"
     allow="accelerometer; ambient-light-sensor; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; vr; xr-spatial-tracking"
     sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"
   ></iframe>

### Update - 更新

渲染的目标添加完以后，和 `Render` 组件一样，配合 `Core` 在需要的时候进行渲染即可。

```javascript
if (losCoreComponent.update()) {
  	// 如果 Core 有更新，则更新渲染
  	// 可以根据自己的逻辑需求，选择性更新
  	losMaskComponent.updateTarget(loSComponentCoreA);
  	losMaskComponent.updateTarget(loSComponentCoreC);
}
```

## Light

通过对 `Core` 组件计算出的 `SightArea` 进行定制的渲染，实现简单的光照效果。

`TBD`

<iframe src="https://codesandbox.io/embed/ssrlos-demo-light-0h9gj?fontsize=14&hidenavigation=1&theme=dark&view=preview"
     style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;"
     title="ssrlos-demo-light"
     allow="accelerometer; ambient-light-sensor; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; vr; xr-spatial-tracking"
     sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"
   ></iframe>

多光源支持

`TBD`

<iframe src="https://codesandbox.io/embed/ssrlos-demo-multi-lights-m2ryx?fontsize=14&hidenavigation=1&theme=dark&view=preview"
     style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;"
     title="ssrlos-demo-multi-lights"
     allow="accelerometer; ambient-light-sensor; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; vr; xr-spatial-tracking"
     sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"
   ></iframe>   

## Shadow [实验版]

通过对 `Core` 组件计算出的 `SightArea` 进行定制的渲染，实现简单的阴影效果。

`TBD`

<iframe src="https://codesandbox.io/embed/ssrlos-demo-shadow-q44qo?fontsize=14&hidenavigation=1&theme=dark&view=preview"
     style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;"
     title="ssrlos-demo-shadow"
     allow="accelerometer; ambient-light-sensor; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; vr; xr-spatial-tracking"
     sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"
   ></iframe>

## Tool

### Visibility - 目标可见性

`TBD`

<iframe src="https://codesandbox.io/embed/ssrlos-demo-visibility-744mo?fontsize=14&hidenavigation=1&theme=dark&view=preview"
     style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;"
     title="ssrlos-demo-visibility"
     allow="accelerometer; ambient-light-sensor; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; vr; xr-spatial-tracking"
     sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"
   ></iframe>

## Helper

一些算法相关的函数，也可以供外部调用。详见 `jsdoc`。
